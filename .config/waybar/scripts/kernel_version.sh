#!/bin/bash

# Get the kernel version using 'uname -r'
kernel_version=$(uname -r)

# Print the kernel version
echo "Kernel Version: $kernel_version"

